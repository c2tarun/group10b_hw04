package com.hw04.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * Group10B_HW04
 * Topic.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class Topic implements Parcelable {

	private long id;
	private int position;
	private String type;
	private String title;
	private String additionalInfo;
	
	public Topic() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	public static Topic createTopic(JSONObject js) throws JSONException {
		Topic topic = new Topic();
		topic.setId(js.getLong("id"));
		topic.setPosition(js.getInt("num"));
		topic.setTitle(js.getJSONObject("title").getString("$text"));
		topic.setType(js.getString("type"));
		topic.setAdditionalInfo(js.getJSONObject("additionalInfo").getString("$text"));
		
		return topic;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.title;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(additionalInfo);
		dest.writeString(type);
		dest.writeLong(id);
		dest.writeInt(position);
	}
	public static final Parcelable.Creator<Topic> CREATOR = new Parcelable.Creator<Topic>() {

		@Override
		public Topic createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Topic(source);
		}

		@Override
		public Topic[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Topic[size];
		}
	};
	
	private Topic(Parcel parcel) {
		this.title = parcel.readString();
		this.additionalInfo = parcel.readString();
		this.type = parcel.readString();
		this.id = parcel.readLong();
		this.position = parcel.readInt();
	}
}
