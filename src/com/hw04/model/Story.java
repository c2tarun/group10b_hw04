package com.hw04.model;

import java.util.Date;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/*
 * Group10B_HW04
 * Story.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class Story implements Parcelable {
	private long id;
	private String storyUrl;
	private String title;
	private String teaser;
	private Date storyPublishDate;
	private StoryAudio audio;
	private String publisherName;
	private String imageUrl;

	public static final String TEXT = "$text";
	public static final String DATE_FORMAT = "EEE, d MMM yyyy HH:mm:ss Z";
	private static final String TAG = Story.class.getName();
	
	public Story() {
		
	}

	public static Story createStory(JSONObject js) {
		Story story = new Story();
		story.setId(getIdFromJSON(js));

		story.setTitle(getTitleFromJSON(js));
		story.setTeaser(getTeaserFromJSON(js));
		story.setStoryPublishDate(getDateFromJSON(js));

		StoryAudio audio = getAudioFromJSON(js);
		if (audio != null)
			story.setAudio(audio);

		story.setPublisherName(getPublisherNameFromJSON(js));
		story.setImageUrl(getImageUrlFromJSON(js));

		story.setStoryUrl(getStoryUrlFromJSON(js));
		return story;
	}

	private static String getStoryUrlFromJSON(JSONObject js) {
		try {
			JSONArray linkArray = js.getJSONArray("link");
			for (int i = 0; i < linkArray.length(); i++) {
				if (linkArray.getJSONObject(i).getString("type").equals("html")) {
					return linkArray.getJSONObject(i).getString(TEXT);
				}
			}
		} catch (JSONException e) {
			Log.d(TAG, "Story url missing");
			return "";
		}
		return "";
	}

	private static long getIdFromJSON(JSONObject js) {
		try {
			return js.getLong("id");
		} catch (JSONException e) {
			Log.d(TAG, "id is missing");
			return 0;
		}
	}

	private static String getImageUrlFromJSON(JSONObject js) {
		try {
			return js.getJSONArray("image").getJSONObject(0).getString("src");
		} catch (JSONException e) {
			Log.d(TAG, "Image url missing");
			return "";
		}
	}

	private static String getPublisherNameFromJSON(JSONObject js) {
		try {
			return js.getJSONArray("byline").getJSONObject(0).getJSONObject("name").getString(TEXT);
		} catch (JSONException e) {
			Log.d(TAG, "Publisher name missing");
			return "";
		}
	}

	private static StoryAudio getAudioFromJSON(JSONObject js) {
		StoryAudio audio = new StoryAudio();
		try {
			if (js.has("audio")) {
				JSONArray audioArray = js.getJSONArray("audio");
				for (int i = 0; i < audioArray.length(); i++) {
					JSONObject obj = audioArray.getJSONObject(i);
					if (obj.has("format")) {
						if (obj.getJSONObject("format").has("mp3")) {
							JSONObject obj2 = obj.getJSONObject("format").getJSONArray("mp3").getJSONObject(0);
							if (obj2.getString("type").equals("m3u")) {
								audio.setUrl(obj2.getString(TEXT));
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Log.d(TAG, "Audio data is not good");
			return null;
		}
		return audio;
	}

	private static Date getDateFromJSON(JSONObject js) {
		String date = "";
		try {
			date = js.getJSONObject("storyDate").getString(TEXT);
		} catch (JSONException e1) {
			Log.d(TAG, "Story date missing");
		}
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {

			return new Date();
		}
	}

	private static String getTeaserFromJSON(JSONObject js) {
		try {
			return js.getJSONObject("teaser").getString(TEXT);
		} catch (JSONException e) {
			Log.d(TAG, "Teaser missing");
			return "";
		}
	}

	private static String getTitleFromJSON(JSONObject js) {
		try {
			return js.getJSONObject("title").getString(TEXT);
		} catch (JSONException e) {
			Log.d(TAG, "Title missing in JSON");
			return "";
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStoryUrl() {
		return storyUrl;
	}

	public void setStoryUrl(String storyUrl) {
		this.storyUrl = storyUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTeaser() {
		return teaser;
	}

	public void setTeaser(String teaser) {
		this.teaser = teaser;
	}

	public Date getStoryPublishDate() {
		return storyPublishDate;
	}

	public void setStoryPublishDate(Date storyPublishDate) {
		this.storyPublishDate = storyPublishDate;
	}

	public StoryAudio getAudio() {
		return audio;
	}

	public void setAudio(StoryAudio audio) {
		this.audio = audio;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append(title + "\n");
		sb.append(imageUrl + "\n");
		sb.append(storyUrl + "\n");
		return sb.toString();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(audio, flags);
		dest.writeLong(id);
		dest.writeString(storyUrl);
		dest.writeString(title);
		dest.writeString(teaser);
		dest.writeLong(storyPublishDate.getTime());
		dest.writeString(publisherName);
		dest.writeString(imageUrl);
	}
	
	public static final Parcelable.Creator<Story> CREATOR = new Parcelable.Creator<Story>() {
		
		@Override
		public Story createFromParcel(Parcel source) {
			return new Story(source);
		}
		
		@Override
		public Story[] newArray(int size) {
			return new Story[size];
		}
	};
	
	/*
	private StoryAudio audio;
	 * private long id;
	private String storyUrl;
	private String title;
	private String teaser;
	private Date storyPublishDate;
	private String publisherName;
	private String imageUrl;*/
	private Story(Parcel parcel) {
		this.audio = parcel.readParcelable(StoryAudio.class.getClassLoader());
		this.id = parcel.readLong();
		this.storyUrl = parcel.readString();
		this.title = parcel.readString();
		this.teaser = parcel.readString();
		this.storyPublishDate = new Date(parcel.readLong());
		this.publisherName = parcel.readString();
		this.imageUrl = parcel.readString();
	}

}
