package com.hw04.model;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * Group10B_HW04
 * StoryAudio.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class StoryAudio implements Parcelable{
	private long id;
	private String url;
	
	public StoryAudio() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeLong(id);
		dest.writeString(url);
		
	}

	public static final Parcelable.Creator<StoryAudio> CREATOR = new Parcelable.Creator<StoryAudio>() {
		
		@Override
		public StoryAudio createFromParcel(Parcel source) {
			return new StoryAudio(source);
		}
		
		@Override
		public StoryAudio[] newArray(int size) {
			return new StoryAudio[size];
		}
	};
	
	private StoryAudio(Parcel parcel) {
		this.id = parcel.readLong();
		this.url = parcel.readString();
	}
	
}
