package com.hw04.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/*
 * Group10B_HW04
 * JSONUtil.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class JSONUtil {
	private static final String TAG = JSONUtil.class.getName();
	
	public static class TopicJSONParser {
		public static List<Topic> parseTopics(String in) throws JSONException {
			List<Topic> topics = new ArrayList<Topic>();
			
			JSONObject root = new JSONObject(in);
			JSONArray topicJSONArray = root.getJSONArray("item");
			for(int i = 0;i<topicJSONArray.length(); i++) {
				Topic topic = Topic.createTopic(topicJSONArray.getJSONObject(i));
				topics.add(topic);
			}
			return topics;
		}
	}
	
	public static class StoryJSONParser {
		public static List<Story> parseStories(String in) throws JSONException {
			List<Story> stories = new ArrayList<Story>();
			
			JSONObject root = new JSONObject(in);
			JSONArray storyJSONArray = root.getJSONObject("list").getJSONArray("story");
			for (int i = 0; i < storyJSONArray.length(); i++) {
				Story story = Story.createStory(storyJSONArray.getJSONObject(i));
				stories.add(story);
			}
			return stories;
		}
		
		public static Story parseStory(String in) throws JSONException {
			return parseStories(in).get(0);
		}
		
	}

}
