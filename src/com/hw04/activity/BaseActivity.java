package com.hw04.activity;

import com.hw04.utils.PreferenceUtils;

import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class BaseActivity extends Activity {
	
	private static final String TAG = BaseActivity.class.getName();
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		Log.d(TAG,"OnCreateOptionsMenu called ");
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.overflow_menu_layout, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getItemId() == R.id.clear_history) {
			PreferenceUtils pref = new PreferenceUtils(getApplicationContext());
			pref.clearHistory();
			Toast.makeText(BaseActivity.this, "Favourites Cleared", Toast.LENGTH_LONG).show();
		}
		return super.onOptionsItemSelected(item);
	}

}
