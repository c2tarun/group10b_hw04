package com.hw04.activity;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import com.hw04.adapter.StoryListAdapter;
import com.hw04.asynctasks.FetchAudioAsyncTask;
import com.hw04.asynctasks.FetchAudioAsyncTask.FetchAudio;
import com.hw04.model.Story;
import com.hw04.utils.PreferenceUtils;

/*
 * Group10B_HW04
 * StoryActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class StoryActivity extends BaseActivity implements FetchAudio, MediaController.MediaPlayerControl {

	TextView titleView;
	TextView publisherName;
	TextView dateView;

	ImageView back;
	ImageView website;
	ImageView audio;
	ImageView favorite;

	TextView teaser;

	String audioUrl;
	Story story;

	MediaPlayer mplayer;
	MediaController mcontroller;

	ProgressDialog pd;

	private static final String TAG = StoryActivity.class.getName();
	PreferenceUtils preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_story);

		titleView = (TextView) findViewById(R.id.storyTitle);
		publisherName = (TextView) findViewById(R.id.storyPublisherName);
		dateView = (TextView) findViewById(R.id.storyDate);

		back = (ImageView) findViewById(R.id.backImage);
		website = (ImageView) findViewById(R.id.webImage);
		audio = (ImageView) findViewById(R.id.audioImage);
		favorite = (ImageView) findViewById(R.id.favouriteImage);

		teaser = (TextView) findViewById(R.id.storyTeaser);
		mcontroller = new MediaController(this);
		preferences = new PreferenceUtils(getApplicationContext());

		if (getIntent().getExtras() != null) {
			story = (Story) getIntent().getExtras().get(StoriesActivity.STORY);
			titleView.setText(story.getTitle());
			publisherName.setText(story.getPublisherName());
			SimpleDateFormat sdf = new SimpleDateFormat(StoryListAdapter.STORY_DATE_FORMAT);
			dateView.setText(sdf.format(story.getStoryPublishDate()));

			teaser.setText(story.getTeaser());
		}

		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		website.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (story.getStoryUrl() != null && story.getStoryUrl().length() > 0) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(story.getStoryUrl()));
					startActivity(browserIntent);
				} else {
					Toast.makeText(StoryActivity.this, "Story Url is missing", Toast.LENGTH_LONG).show();
				}
			}
		});

		audio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mplayer == null || !mplayer.isPlaying()) {
					if (story.getAudio() != null) {
						new FetchAudioAsyncTask(getApplicationContext(), StoryActivity.this).execute(story.getAudio()
								.getUrl());
					} else {
						Toast.makeText(StoryActivity.this, "No audio present", Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(StoryActivity.this, "Audio already playing", Toast.LENGTH_LONG).show();
				}
			}
		});

		favorite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (preferences.isExists(story.getId())) {
					preferences.delete(story.getId());
					favorite.setImageResource(R.drawable.rating_not_important);
				} else {
					preferences.save(story.getId());
					favorite.setImageResource(R.drawable.rating_important);
				}
			}
		});

		if (story != null) {
			if (preferences.isExists(story.getId())) {
				favorite.setImageResource(R.drawable.rating_important);
			}

		}

	}

	@Override
	public void startFetchAudio() {
		pd = new ProgressDialog(StoryActivity.this);
		pd.setTitle(getResources().getString(R.string.loading_title));
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setCancelable(false);
		pd.setMessage(getResources().getString(R.string.loading_audio));
		pd.show();
	}

	@Override
	public void playAudio(MediaPlayer mplayer) {
		this.mplayer = mplayer;
		if (mplayer != null) {
			mplayer.start();
			mcontroller.setEnabled(true);
			mcontroller.setMediaPlayer(this);
			mcontroller.setAnchorView(findViewById(android.R.id.content));
			mcontroller.show();
		} else {
			Toast.makeText(StoryActivity.this, "No audio present", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void stopFetchAudio() {
		if (pd != null) {
			pd.dismiss();
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mcontroller.show();
		return false;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		if (mcontroller != null)
			mcontroller.hide();
		if (mplayer != null) {
			mplayer.stop();
			mplayer.reset();
			mplayer.release();
		}
		super.finish();
	}

	// --------- Media Controller methods -----------//
	@Override
	public void start() {
		mplayer.start();
	}

	@Override
	public void pause() {
		mplayer.pause();
	}

	@Override
	public int getDuration() {
		return mplayer.getDuration();
	}

	@Override
	public int getCurrentPosition() {
		return mplayer.getCurrentPosition();
	}

	@Override
	public void seekTo(int pos) {
		mplayer.seekTo(pos);
	}

	@Override
	public boolean isPlaying() {
		return mplayer.isPlaying();
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}

	@Override
	public int getAudioSessionId() {
		return mplayer.getAudioSessionId();
	}

}
