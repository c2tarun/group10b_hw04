package com.hw04.activity;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.hw04.adapter.TopicListAdapter;
import com.hw04.asynctasks.FetchTopicsAsyncTask;
import com.hw04.model.Topic;

/*
 * Group10B_HW04
 * ListActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class ListActivity extends BaseActivity implements FetchTopicsAsyncTask.FetchTopics {
	
	ListView topicsListView;
	ProgressDialog pd;
	List<Topic> tpItems;
	
	public static final String TP_ITEM = "Topic";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		
		topicsListView = (ListView) findViewById(R.id.topicsListView);
		String id = getIntent().getExtras().getString("id");
		FetchTopicsAsyncTask asyncTask = new FetchTopicsAsyncTask(this);
		asyncTask.execute("http://api.npr.org/list","id",id,"output","JSON");
		
		topicsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ListActivity.this, StoriesActivity.class);
				intent.putExtra(TP_ITEM, tpItems.get(position));
				intent.putExtra(MainActivity.ACTION, MainActivity.LIST_ACTION);
				startActivity(intent);
			}
		});
	}

	@Override
	public void enableProgressDialog() {
		// TODO Auto-generated method stub
		pd = new ProgressDialog(ListActivity.this);
		pd.setTitle(getResources().getString(R.string.loading_title));
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setCancelable(false);
		pd.setMessage(getResources().getString(R.string.loading_results));
		pd.show();
		
	}

	@Override
	public void setupTopicList(List<Topic> result) {
		// TODO Auto-generated method stub
		this.tpItems = result;
		TopicListAdapter adapter = new TopicListAdapter(this, R.layout.topic_list_adapter, result);
		topicsListView.setAdapter(adapter);
	}

	@Override
	public void disableProgressDialog() {
		// TODO Auto-generated method stub
		if(pd != null)
			pd.dismiss();
		
	}
}
