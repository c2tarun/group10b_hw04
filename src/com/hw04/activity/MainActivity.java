package com.hw04.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/*
 * Group10B_HW04
 * MainActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class MainActivity extends BaseActivity {
	
	Button programsButton;
	Button topicsButton;
	Button favButton;
	Button quitButton;
	public static final String ACTION = "Action";
	public static final String FAV_ACTION = "for favourite action";
	public static final String LIST_ACTION = "for stories list action";
	
	private static final String TAG = MainActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		topicsButton = (Button) findViewById(R.id.topicsButton);
		topicsButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, ListActivity.class);
				intent.putExtra("id", "3002");
				startActivity(intent);
			}
		});
		programsButton = (Button) findViewById(R.id.programsButton);
		programsButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, ListActivity.class);
				intent.putExtra("id", "3004");
				startActivity(intent);
			}
		});
		
		favButton = (Button) findViewById(R.id.favouriteButton);
		favButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, StoriesActivity.class);
				intent.putExtra(ACTION, FAV_ACTION);
				startActivity(intent);
			}
		});
		
		quitButton = (Button) findViewById(R.id.quitButton);
		quitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
}
