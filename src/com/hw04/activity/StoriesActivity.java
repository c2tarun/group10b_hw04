package com.hw04.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hw04.adapter.StoryListAdapter;
import com.hw04.asynctasks.FetchFavStoriesAsyncTask;
import com.hw04.asynctasks.FetchStoryAsyncTask;
import com.hw04.asynctasks.FetchStoryAsyncTask.FetchStories;
import com.hw04.model.Story;
import com.hw04.model.Topic;
import com.hw04.utils.GeneralUtil;
import com.hw04.utils.PreferenceUtils;

/*
 * Group10B_HW04
 * StoriesActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class StoriesActivity extends BaseActivity implements FetchStories {

	ProgressDialog pd;
	List<Story> stories;
	ListView storiesListView;

	public static final String STORY = "Story";
	private static final String TAG = StoriesActivity.class.getName();
	public static final int STORY_LIST = 10;
	public static final int FAV_LIST = 20;
	
	String action;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stories);

		Log.d(TAG, "On Create called");

		storiesListView = (ListView) findViewById(R.id.storiesListView);
		Topic topic;
		if (getIntent().getExtras() != null) {
			action = (String) getIntent().getExtras().get(MainActivity.ACTION);
			if (MainActivity.LIST_ACTION.equals(action)) {
				topic = (Topic) getIntent().getExtras().get(ListActivity.TP_ITEM);
				new FetchStoryAsyncTask(this).execute("http://api.npr.org/query", "id", topic.getId() + "", "fields",
						"title,teaser,storyDate,byline,audio,image", "output", "JSON", "apiKey",
						"MDE4MzAyODUwMDE0MjQxNzY4MDI5ZjUxYQ001", "numResults", "25");
			} else if (MainActivity.FAV_ACTION.equals(action)) {
				fetchStoriesFromPref();
			}
		}

		storiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(StoriesActivity.this, StoryActivity.class);
				intent.putExtra(STORY, stories.get(position));
				String action = (String) getIntent().getExtras().get(MainActivity.ACTION);
				if(MainActivity.LIST_ACTION.equals(action)){
					startActivityForResult(intent, STORY_LIST);
				} else if (MainActivity.FAV_ACTION.equals(action)){
					startActivityForResult(intent, FAV_LIST);
				}
			}
		});
	}

	private void fetchStoriesFromPref() {
		PreferenceUtils pref = new PreferenceUtils(getApplicationContext());
		String[] idsArray = pref.getAll();
		StringBuilder idCsv = new StringBuilder();
		for (int i = 0; i < idsArray.length; i++) {
			String id = idsArray[i];
			if (i == 0)
				idCsv.append(id);
			else
				idCsv.append("," + id);
		}
		if (!GeneralUtil.isEmpty(idCsv.toString())){
			new FetchFavStoriesAsyncTask(this).execute("http://api.npr.org/query", idCsv.toString(), "fields",
					"title,teaser,storyDate,byline,audio,image", "output", "JSON", "apiKey",
					"MDE4MzAyODUwMDE0MjQxNzY4MDI5ZjUxYQ001");
		} else {
			setupStoryList(new ArrayList<Story>());
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == FAV_LIST) {
			fetchStoriesFromPref();
		}
	}

	@Override
	public void enableProgressDialog() {
		// TODO Auto-generated method stub
		pd = new ProgressDialog(StoriesActivity.this);
		pd.setTitle(getResources().getString(R.string.loading_title));
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setCancelable(false);
		pd.setMessage(getResources().getString(R.string.loading_stories));
		pd.show();
	}

	@Override
	public void setupStoryList(List<Story> result) {
		// TODO Auto-generated method stub
		this.stories = result;
		ArrayAdapter<Story> adapter = new StoryListAdapter(this, R.layout.story_list_adapter, stories);
		storiesListView.setAdapter(adapter);
	}

	@Override
	public void disableProgressDialog() {
		// TODO Auto-generated method stub
		if (pd != null)
			pd.dismiss();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean clear = super.onOptionsItemSelected(item);
		if(MainActivity.FAV_ACTION.equals(action)) {
			fetchStoriesFromPref();
		}
		return clear;
	}
}
