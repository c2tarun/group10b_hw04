package com.hw04.asynctasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONException;

import com.hw04.asynctasks.FetchStoryAsyncTask.FetchStories;
import com.hw04.model.JSONUtil;
import com.hw04.model.Story;
import com.hw04.utils.HttpUtil;

import android.os.AsyncTask;
import android.util.Log;

/*
 * Group10B_HW04
 * FetchFavStoriesAsyncTask.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class FetchFavStoriesAsyncTask extends AsyncTask<String, Void, List<Story>> {

	private static final String TAG = FetchFavStoriesAsyncTask.class.getName();
	
	FetchStories receiver;
	
	public FetchFavStoriesAsyncTask(FetchStories receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		receiver.enableProgressDialog();
	}
	/**
	 * First params should be baseUrl
	 * Second params should be csv of all the story Ids
	 * third onwards should contain all the parameter to fetch story
	 * params
	 * 0 -> baseUrl
	 * 1 -> CSV of story ids
	 * 2.. -> all key value of parameters
	 * 
	 * newParams
	 * 0 -> baseUrl
	 * 1 -> "id"
	 * 2 -> id from array
	 * 3.. -> all key value of parameters
	 */
	@Override
	protected List<Story> doInBackground(String... params) {
		// TODO Auto-generated method stub
		List<Story> stories = new ArrayList<Story>();
		String baseUrl = params[0];
		String[] ids = params[1].split(",");
		String[] newParams = new String[params.length + 1];
		newParams[0] = baseUrl;
		newParams[1] = "id";
		for(int i = 2 ; i< params.length; i++) {
			newParams[i+1] = params[i];
		}
		for(String id: ids) {
			newParams[2] = id;
			String storyJSON = HttpUtil.getStringFromURL(newParams);
			try {
				Story story = JSONUtil.StoryJSONParser.parseStory(storyJSON);
				stories.add(story);
				Log.d(TAG,story.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return stories;
	}
	
	@Override
	protected void onPostExecute(List<Story> result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		receiver.disableProgressDialog();
		receiver.setupStoryList(result);
	}

}
