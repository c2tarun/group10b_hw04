package com.hw04.asynctasks;

import com.hw04.utils.GeneralUtil;
import com.hw04.utils.HttpUtil;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;

/*
 * Group10B_HW04
 * FetchAudioAsyncTask.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class FetchAudioAsyncTask extends AsyncTask<String, Void, MediaPlayer> {
	
	FetchAudio receiver;
	MediaPlayer mplayer;
	Context context;
	
	public FetchAudioAsyncTask(Context context, FetchAudio receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
		this.context = context;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		receiver.startFetchAudio();
	}

	@Override
	protected MediaPlayer doInBackground(String... params) {
		String audioUrl = HttpUtil.getStringFromURL(params[0]);
		if(!GeneralUtil.isEmpty(audioUrl)){
			mplayer = MediaPlayer.create(context, Uri.parse(audioUrl));
			return mplayer;
		} else {
			return null;
		}
	}
	
	@Override
	protected void onPostExecute(MediaPlayer mplayer) {
		super.onPostExecute(mplayer);
		receiver.playAudio(mplayer);
		receiver.stopFetchAudio();
	}
	
	public interface FetchAudio {
		public void startFetchAudio();
		
		public void playAudio(MediaPlayer mplayer);
		
		public void stopFetchAudio();
	}

}
