package com.hw04.asynctasks;

import java.util.HashMap;
import java.util.List;

import org.json.JSONException;

import android.os.AsyncTask;
import android.util.Log;

import com.hw04.model.Story;
import com.hw04.model.Topic;
import com.hw04.model.JSONUtil;
import com.hw04.utils.HttpUtil;

/*
 * Group10B_HW04
 * FetchStoryAsyncTask.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class FetchStoryAsyncTask extends AsyncTask<String, Void, List<Story>> {
	private static final String TAG = FetchStoryAsyncTask.class.getName();
	HashMap<String, String> urlParams;
	FetchStories receiver;
	
	public FetchStoryAsyncTask(FetchStories receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		urlParams = new HashMap<String, String>();
		receiver.enableProgressDialog();
	}

	@Override
	protected List<Story> doInBackground(String... input) {
		String json = HttpUtil.getStringFromURL(input);
		try {
			return JSONUtil.StoryJSONParser.parseStories(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(List<Story> result) {
		receiver.disableProgressDialog();
		receiver.setupStoryList(result);
	}

	public interface FetchStories {
		public void enableProgressDialog();

		public void setupStoryList(List<Story> topics);

		public void disableProgressDialog();
	}

}
