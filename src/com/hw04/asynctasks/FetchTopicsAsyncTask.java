package com.hw04.asynctasks;

import java.util.HashMap;
import java.util.List;

import org.json.JSONException;

import android.os.AsyncTask;
import android.util.Log;

import com.hw04.model.Topic;
import com.hw04.model.JSONUtil;
import com.hw04.utils.HttpUtil;

/*
 * Group10B_HW04
 * FetchTopicsAsyncTask.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class FetchTopicsAsyncTask extends AsyncTask<String, Void, List<Topic>> {
	private static final String TAG = FetchTopicsAsyncTask.class.getName();
	HashMap<String, String> urlParams;
	FetchTopics receiver;
	
	public FetchTopicsAsyncTask(FetchTopics receiver) {
		// TODO Auto-generated constructor stub
		this.receiver = receiver;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		urlParams = new HashMap<String, String>();
		receiver.enableProgressDialog();
	}

	@Override
	protected List<Topic> doInBackground(String... input) {
		String json = HttpUtil.getStringFromURL(input);
		try {
			return JSONUtil.TopicJSONParser.parseTopics(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(List<Topic> topics) {
		for(Topic topic : topics) {
			Log.d(TAG, topic.toString());
		}
		receiver.disableProgressDialog();
		receiver.setupTopicList(topics);
	}

	public interface FetchTopics {
		public void enableProgressDialog();

		public void setupTopicList(List<Topic> topics);

		public void disableProgressDialog();
	}

}
