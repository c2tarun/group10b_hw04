package com.hw04.adapter;

import java.text.SimpleDateFormat;
import java.util.List;

import com.hw04.activity.R;
import com.hw04.model.Story;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/*
 * Group10B_HW04
 * StoryListAdapter.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class StoryListAdapter extends ArrayAdapter<Story> {

	Context context;
	int resource;
	List<Story> stories;
	public static final String STORY_DATE_FORMAT = "EEE, d MMM yyyy";

	public StoryListAdapter(Context context, int resource, List<Story> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.resource = resource;
		this.stories = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Story story = stories.get(position);

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(resource, parent, false);
			ViewHolder holder = new ViewHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.storyListImage);
			holder.titleTextView = (TextView) convertView.findViewById(R.id.storyListTitle);
			holder.dateTextView = (TextView) convertView.findViewById(R.id.storyListPublicationDate);
			holder.teaserTextView = (TextView) convertView.findViewById(R.id.storyListTeaserView);
			convertView.setTag(R.id.HOLDER, holder);
		}

		ViewHolder holder = (ViewHolder) convertView.getTag(R.id.HOLDER);
		String imageUrl = story.getImageUrl();
		if (imageUrl != null && imageUrl.trim().length() > 0)
			Picasso.with(context).load(story.getImageUrl()).into(holder.imageView);
		else
			holder.imageView.setImageResource(R.drawable.no_image);
		holder.titleTextView.setText(story.getTitle());
		holder.teaserTextView.setText(story.getTeaser());
		SimpleDateFormat sdf = new SimpleDateFormat(STORY_DATE_FORMAT);
		holder.dateTextView.setText(sdf.format(story.getStoryPublishDate()));

		return convertView;
	}

	private class ViewHolder {
		ImageView imageView;
		TextView titleTextView;
		TextView dateTextView;
		TextView teaserTextView;
	}

}
