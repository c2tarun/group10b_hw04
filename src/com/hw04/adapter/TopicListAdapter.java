package com.hw04.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hw04.activity.R;
import com.hw04.model.Topic;

/*
 * Group10B_HW04
 * TopicListAdapter.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class TopicListAdapter extends ArrayAdapter<Topic>{
	
	private Context context;
	private int resource;
	private List<Topic> topics;

	public TopicListAdapter(Context context, int resource, List<Topic> objects) {
		super(context, resource, objects);
		this.context = context;
		this.resource = resource;
		this.topics = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(resource, parent, false);
		}
		Topic topic = topics.get(position);
		TextView titleText = (TextView) convertView.findViewById(R.id.titleTextView);
		titleText.setText(topic.toString());
		return convertView;
	}

}
