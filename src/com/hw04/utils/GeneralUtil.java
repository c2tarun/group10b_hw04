package com.hw04.utils;

/*
 * Group10B_HW04
 * GeneralUtil.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public final class GeneralUtil {
	
	public static boolean isEmpty(String str) {
		if(str == null)
			return true;
		if(str.trim().length() == 0)
			return true;
		return false;
	}

}
