package com.hw04.utils;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/*
 * Group10B_HW04
 * PreferenceUtils.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class PreferenceUtils {
	
	Context context;
	private static final String TAG = PreferenceUtils.class.getSimpleName();
	
	private static final String PREF_NAME = "hw04";
	private static final String FAVOURITES = "favourites";
	private static final String DELIM = " ";
	
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	
	public PreferenceUtils(Context context) {
		this.context = context;
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		editor = preferences.edit();
		
	}
	
	public void save(long idToSave) {
		String ids = preferences.getString(FAVOURITES, "");
		if(!isExists(idToSave)){
			ids = ids + DELIM + idToSave;
			editor.putString(FAVOURITES, ids.trim());
		}
		
		if(editor.commit()) {
			Log.d(TAG,idToSave + " saved");
		} else {
			Log.d(TAG,idToSave + " save failed");
		}
	}
	
	public void delete(long idToDelete) {
		String ids = preferences.getString(FAVOURITES, "");
		String newIds = "";
		String[] idsSplit = ids.split(DELIM);
		for(String id : idsSplit) {
			if(id.equals(idToDelete + ""))
				continue;
			newIds = newIds + id + DELIM;
		}
		editor.putString(FAVOURITES, newIds.trim());
		editor.commit();
		Log.d(TAG,idToDelete + " deleted");
		Log.d(TAG,newIds.trim() + " ids after deletion");
	}
	
	public boolean isExists(long idToSearch) {
		String[] idsSplit = preferences.getString(FAVOURITES, "").split(DELIM);
		for(String id : idsSplit) {
			if(id.equals(idToSearch + ""))
				return true;
		}
		return false;
	}
	
	public String[] getAll() {
		String[] ids = preferences.getString(FAVOURITES, "").split(DELIM);
		Log.d(TAG,ids.length + "");
		
		return ids;
	}
	
	public void clearHistory() {
		editor.putString(FAVOURITES, "");
		editor.clear();
		editor.commit();
	}

}
